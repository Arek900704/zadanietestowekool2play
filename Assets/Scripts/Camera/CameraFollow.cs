﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    Vector3 distance;
    public Transform playerTransform;

    void Start()
    {
        distance = transform.position - playerTransform.position;
    }

  
    void Update()
    {
        transform.position = playerTransform.position + distance;
    }
}
