﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{

    public GameObject enemyToSpawn;
    float timer = 0;
    
    

    // Update is called once per frame
    void Update()
    {
        timer += Time.unscaledDeltaTime;
        
        if(timer>5f)
        {
            enemyToSpawn.transform.position = new Vector3(Random.Range(-10.0f, 10.0f), 1.0f, Random.Range(-10.0f, 10.0f));
            Instantiate(enemyToSpawn);
            timer = 0;
        }
    }
}
