﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehavior : MonoBehaviour
{
    private const int GUN = 0;
    private const int SHOTGUN = 1;

    KillEnemyCounter killCounter;
    public int heath = 25;
    public float speed = 0.5f;
    public float minDistance = 2f;
    GameObject player;
    float step;
    float timeBetweenHit = 1.0f;
    Vector3 distance;


    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        killCounter = GameObject.FindGameObjectWithTag("KillCounter").GetComponent<KillEnemyCounter>();
    }


    void Update()
    {
        step = speed * Time.deltaTime;
        distance = player.transform.position - gameObject.transform.position;
        if(distance.magnitude<minDistance)
        {
            timeBetweenHit -= Time.deltaTime;
            if(timeBetweenHit<0)
            {
                player.SendMessage("Damage");
                timeBetweenHit = 1.0f;
            }
            
        }
        else
        {
            transform.position = Vector3.MoveTowards(transform.position, player.transform.position, step);
            transform.LookAt(player.transform);
        }

       if(heath<=0)
        {
            killCounter.killedEnemies++;
            Destroy(gameObject);
        }
            
    }

    public void DamageByGun(int gunType)
    {
        switch(gunType)
        {
            case GUN:
                {
                    heath -= 15;
                    break;
                }

            case SHOTGUN:
                {
                    heath-=5;
                    break;

                }
        }
    }
}
