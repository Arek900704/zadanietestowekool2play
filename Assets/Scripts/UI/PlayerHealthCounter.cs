﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealthCounter : MonoBehaviour
{
    public PlayerBehavior playerBehawior;
    Text text;
    
    void Start()
    {
        text = gameObject.GetComponent<Text>();
    }

   
    void Update()
    {
        text.text = string.Concat("Health: ", playerBehawior.health);
    }
}
