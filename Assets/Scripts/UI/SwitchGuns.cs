﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SwitchGuns : MonoBehaviour
{

    private const int GUN = 0;
    private const int SHOTGUN = 1;

    public Sprite gunImage;
    public Sprite shotgunImage;

    public void SwitchGunsImage(int guntype)
    {
        switch(guntype)
        {
            case GUN:
                {
                    gameObject.GetComponent<Image>().sprite = gunImage;
                    break;
                }
            case SHOTGUN:
                {
                    gameObject.GetComponent<Image>().sprite = shotgunImage;
                    break;
                }
        }
    }
}
