﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KillEnemyCounter : MonoBehaviour
{
    public int killedEnemies = 0;
    Text text;
    
    void Start()
    {
        text = gameObject.GetComponent<Text>();
    }

   
    void Update()
    {
        text.text = string.Concat("Killed: ", killedEnemies);
    }
}
