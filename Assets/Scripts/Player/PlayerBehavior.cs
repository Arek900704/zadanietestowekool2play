﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerBehavior : MonoBehaviour
{
    private const int GUN = 0;
    private const int SHOTGUN = 1;


    Ray cameraRay;
    Plane groundPlane = new Plane(Vector3.up, Vector3.zero);
    float raycastLenght;
    Vector3 positionToLook;
    ParticleSystem shot;
    int currentWeapon = GUN;

    public int speed = 5;
    public int health = 100;
    public GameObject handWithGun;
    public Image gunImage;
    public GameObject deathText;

    // Start is called before the first frame update
    void Start()
    {
        shot = handWithGun.transform.GetChild(currentWeapon).transform.GetChild(2).gameObject.GetComponent<ParticleSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        cameraRay = Camera.main.ScreenPointToRay(Input.mousePosition);

        if(groundPlane.Raycast(cameraRay,out raycastLenght))
        {
            positionToLook = cameraRay.GetPoint(raycastLenght);
            transform.LookAt(new Vector3(positionToLook.x, transform.position.y, positionToLook.z));
        }

        if(health<=0)
        {
            deathText.SetActive(true);
        }

        if(Input.GetKey(KeyCode.W))
        {
            transform.Translate(Vector3.forward *speed* Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(Vector3.left *speed* Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(Vector3.right *speed* Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(Vector3.back *speed* Time.deltaTime);
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            handWithGun.transform.GetChild(currentWeapon).gameObject.SetActive(false);
            if (currentWeapon == GUN)
            {
                currentWeapon = SHOTGUN;
            }else
            {
                currentWeapon = GUN;
            }
            handWithGun.transform.GetChild(currentWeapon).gameObject.SetActive(true);
            shot = handWithGun.transform.GetChild(currentWeapon).transform.GetChild(2).gameObject.GetComponent<ParticleSystem>();
            gunImage.SendMessage("SwitchGunsImage", currentWeapon);
        }

        if (Input.GetMouseButtonDown(0))
        {
            shot.Play();
        }

        if (Input.GetMouseButtonUp(0))
        {
            if(currentWeapon == GUN)
            {
                shot.Stop();
            }
        }

    }

    public void Damage()
    {
        health -= 5;
    }
}
