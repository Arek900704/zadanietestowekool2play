﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootDetection : MonoBehaviour
{
    public bool Shotgun;
    public bool Gun;
    private const int GUN = 0;
    private const int SHOTGUN = 1;

    public List<ParticleCollisionEvent> collisionEvents;
    public ParticleSystem particle;

    private void Start()
    {
        particle = gameObject.GetComponent<ParticleSystem>();
        collisionEvents = new List<ParticleCollisionEvent>();
    }

    private void OnParticleCollision(GameObject other)
    {
        int numCollisionEvents = particle.GetCollisionEvents(other, collisionEvents);
        Debug.Log(other.name);
        if (Shotgun)
        {


            for (int i = 0; i < numCollisionEvents; i++)
                other.GetComponent<EnemyBehavior>().DamageByGun(SHOTGUN);
        }
        if (Gun)
        {
            for (int i = 0; i < numCollisionEvents; i++)
                other.GetComponent<EnemyBehavior>().DamageByGun(GUN);
        }

    }
}
